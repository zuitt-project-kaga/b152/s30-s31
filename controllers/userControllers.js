const User = require("../models/User");

module.exports.createUserController = (req, res) => {
  console.log(req.body);
  User.findOne({ username: req.body.username })
    .then((result) => {
      if (result !== null && result.username === req.body.username) {
        return res.send("Duplicate User Found");
      } else {
        let newUser = new User({
          username: req.body.username,
          password: req.body.password,
        });
        newUser
          .save()
          .then((result) => res.send(result))
          .catch((error) => res.send(error));
      }
    })
    .catch((error) => res.send(error));
};

module.exports.getAllUsersController = (req, res) => {
  User.find({})
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.updateUsernameController = (req, res) => {
  let update = {
    username: req.body.username,
  };
  User.findByIdAndUpdate(req.params.id, update, { new: true })
    .then((updateUsername) => res.send(updateUsername))
    .catch((error) => res.send(error));
};

module.exports.getSingleUserController = (req, res) => {
  User.findById(req.params.id)
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};
