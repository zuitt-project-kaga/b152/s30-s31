const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect(
  "mongodb+srv://masahirokaga:masaMongo@cluster0.mx6qv.mongodb.net/task152?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error."));
db.once("open", () => console.log("Connected to MongoDB"));

app.use(express.json());

const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes);

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

app.listen(port, () => console.log(`Server running at port ${port}`));
