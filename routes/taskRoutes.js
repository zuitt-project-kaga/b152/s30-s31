const express = require("express");
const router = express.Router();

const taskControllers = require('../controllers/taskControllers');

router.post('/',taskControllers.createTaskController);

router.get('/',taskControllers.getAllTasksController);

// http://localhost:4000/tasks/getSingleTask/61ee3b05d37e07a555be0540
router.get('/getSingleTask/:id',taskControllers.getSingleTaskConroller);

router.put('/updateTaskStatus/:id',taskControllers.updateTaskStatusController);

module.exports = router;